<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPosting extends Model
{
  protected $fillable = [
        'job_name',
        'job_description',
        'vacants'
  ];
}
