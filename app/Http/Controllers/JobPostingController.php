<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobPostingRequest;
use App\JobPosting;
use Illuminate\Http\Request;

class JobPostingController extends Controller
{

    public function index()
    {
        $posting = JobPosting::all();
        return view('job_postings.index', compact('posting'));
    }
    public function create()
    {
        //
    }
    public function store(JobPostingRequest $jobPostingRequest, JobPosting $jobPosting)
    {
        $posting = $jobPosting::create($jobPostingRequest->all());
        return response()->json(['message' => 'Job Posting has been successfully saved', 'data' => $posting]);
    }
    public function show(JobPosting $jobPosting)
    {
        //
    }
    public function edit(JobPosting $jobPosting)
    {
        //
    }

    public function update(JobPostingRequest $jobPostingRequest, JobPosting $jobPosting)
    {
        $posting = $jobPosting->update($jobPostingRequest->all());
        return response()->json(['message' => 'Job Posting has been successfully updated', 'data' => $posting]);
    }
    public function destroy(JobPosting $jobPosting)
    {
        $jobPosting->delete();
        return response()->json(['message' => 'Job Posting has been successfully deleted']);
    }
}
